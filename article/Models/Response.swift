//
//  Response.swift
//  article
//
//  Created by iMac on 3/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import Foundation


struct Response<T: Codable>: Codable {
    
    var CODE    : String?
    var MESSAGE : String?
    var DATA    : [T] = [T]()
    
}
