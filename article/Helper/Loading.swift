//
//  Constant.swift
//  article
//
//  Created by iMac on 3/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import UIKit


public class LoadingView: UIView {
    
    var coachView   : UIView!
    var spinner     : UIActivityIndicatorView!
    
    private var coverView       : UIView?
    private var loadingImageView: UIImageView?
    
    override public var frame: CGRect {
        didSet {
            self.update()
        }
    }
    
    class var sharedInstance: LoadingView {
        struct Singleton {
            static let instance = LoadingView(frame: CGRect(x: 0, y: 0, width: 24 ,height: 27))
        }
        return Singleton.instance
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.alpha = 0.0
        self.backgroundColor = UIColor.clear
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public class func show() {
        
        let currentWindow: UIWindow = UIApplication.shared.keyWindow!
        let view = LoadingView.sharedInstance
        view.backgroundColor = UIColor.clear
        
        let height  : CGFloat = UIScreen.main.bounds.size.height
        let width   : CGFloat = UIScreen.main.bounds.size.width
        let center  : CGPoint = CGPoint(x: width / 2.0, y: height / 2.0)
        
        view.center = center
        view.layer.zPosition = 101
        
        if view.superview == nil {
            view.coverView = UIView(frame: currentWindow.bounds)
            view.coverView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            currentWindow.addSubview(view.coverView!)
            currentWindow.addSubview(view)
            UIView.animate(withDuration: 0.18, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                view.coverView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                view.coverView?.layer.zPosition = 100
            }, completion: nil)
            view.start()
        }
        
    }
    
    public class func hide(statusBarDefault: Bool = true){
        let loadingView = LoadingView.sharedInstance
        loadingView.stop(statusBarColor: statusBarDefault)
    }
    
    private func start(){
        loadingImageView?.startAnimating()
        self.alpha = 1.0
    }
    
    private func stop(statusBarColor: Bool){
        
        if ((coverView?.superview) != nil) {
            UIView.animate(withDuration: 0.18, delay: 0.0, options: UIViewAnimationOptions.curveEaseIn, animations: { [weak self] in
                guard let `self` = self else { return }
                self.coverView?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
                }, completion:{ [weak self] _ in
                    guard let `self` = self else { return }
                    self.coverView?.removeFromSuperview()
                    if self.superview != nil {
                        self.removeFromSuperview()
                    }
                    
                    self.loadingImageView?.stopAnimating()
                    self.alpha = 0.0
            })
        }
    }
    
    public class func delayBeforeHide(seconds: Double) {
        let time = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds)) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: time) {
            hide()
        }
    }
    
    private func update() {
        
        if self.loadingImageView == nil {
            loadingImageView = UIImageView(frame: self.bounds)
            
            var images = [UIImage]()
            
            for index in 1...6 {
                let image = UIImage(named: String(format:"load_%02d", index))
                images.append(image!)
            }
            
            loadingImageView?.animationImages = images
            loadingImageView?.animationDuration = 1.0
            loadingImageView?.animationRepeatCount = 0
            loadingImageView?.center = CGPoint(x: 24 / 2, y: 27 / 2)
            self.addSubview(loadingImageView!)
        }
    }
}
