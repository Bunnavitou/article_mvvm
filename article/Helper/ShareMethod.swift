//
//  Constant.swift
//  article
//
//  Created by iMac on 3/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import Foundation
import UIKit

class ShareMethod {
    static let shared = ShareMethod()
    private init() {}
    
    func formatUserAgent1() -> String {
        let device = UIDevice.current
        let appID = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        
        return "Mozilla/5.0 (\(device.model); U; CPU \(device.systemName) \(device.systemVersion.replacingOccurrences(of: ".", with: "_")) like Mac OS X; ko-kr) AppleWebKit/528.18 (KHTML, like Gecko) Mobile;nma-plf=[IPN];nma-plf-ver=[\(device.systemVersion)];nma-app-id=[\(appID)]"
    }
    
    class func canExecuteApplication(_ urlScheme:String) -> Bool {
        if urlScheme == "" {
            return false
        }
        let url:URL = URL(string: urlScheme)!
        return UIApplication.shared.canOpenURL(url)
    }
    
    //--- Todo check application can execute or not
    class func applicationExecute(_ urlScheme:String) -> Bool {
        if urlScheme != "" {
            let url:URL = URL(string: urlScheme)!
            return UIApplication.shared.openURL(url)
        }
        return false
    }
    
    class func getViewControllerFrom(_ storyboardID:String , viewControllerID:String)->UIViewController!{
        let storyboard = UIStoryboard(name: storyboardID, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: viewControllerID)
        return vc;
    }
    
    
    class func getOSVersion() -> Int {
        let sOSVersion = UIDevice.current.systemVersion.replacingOccurrences(of: ".", with: "0")
        if Int(sOSVersion)! < 10000 {
            // iOS 4.1부터는 버전 번호 자릿수가 3자리여서,
            return Int(sOSVersion)!  * 100
        } else {
            return Int(sOSVersion)!
        }
    }
    
    class func getAppVersion() -> String {
        return Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
    }
}
