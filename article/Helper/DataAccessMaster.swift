//
//  DataAccessMaster.swift
//  article
//
//  Created by iMac on 3/6/18.
//  Copyright © 2018 iMac. All rights reserved.
//

import Foundation
import UIKit

class DataAccessMaster {
    
    //Config singleton
    static let manager = DataAccessMaster()
    
    //Request Timer
    let timeOutDuration : TimeInterval = 59.0
    var timer           : Timer?
    var isTimeout = false
    
    private init(){}
    
    private func request(urlApi: String, body: [String: AnyObject]) -> URLRequest {
        var url: URL!
        var request: URLRequest!
        let dataRequest = ["KEY":"VALUE"]
        let mgUrl = "URL here"
        url = URL(string: mgUrl)
        request = URLRequest(url: url)
        
        request.httpBody = "\(dataRequest)".data(using: .utf8)
        
        #if DEBUG
        print("\n✂︎----------------------------------------------------------")
        print("From API URL: \(request.url!)")
        print("Request Body:\n\(String(describing: self.JSONStringify(value: dataRequest as AnyObject, prettyPrinted: true)))")
        print("✂︎----------------------------------------------------------\n")
        #endif
  
        
        let userAgent = ShareMethod.shared.formatUserAgent1()
        request.setValue(userAgent, forHTTPHeaderField: "User-Agent")
        
        request.httpMethod = "POST"
        
        return request
    }
    
    func fetchData<T: Codable>(API: String, body: [String: AnyObject], responseType: T.Type, completion: @escaping (_ data: T?, _ error: NSError?) -> Void ) {
        
        // Timeout Configuration
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 60.0
        sessionConfig.timeoutIntervalForResource = 60.0
        let session = URLSession(configuration: sessionConfig)
        timer = Timer.scheduledTimer(timeInterval: timeOutDuration, target: self, selector: #selector(onTimeOut), userInfo: nil, repeats: false)
        
        DispatchQueue.main.async {
            LoadingView.show()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        let request = self.request(urlApi: API, body: body)
        session.dataTask(with: request) { [weak self] data, _, _ in
            guard let `self` = self else { return }
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                LoadingView.hide()
            }
            
            self.timer?.invalidate()
            
            //check if timeout
            if self.isTimeout {
                self.isTimeout = false
                let timeoutError = NSError(domain: "Timeout Error", code: -1, userInfo: [NSLocalizedDescriptionKey : "Timeout occurred during the communication."])
                DispatchQueue.main.async {
                    completion(nil, timeoutError)
                }
                return
            }
            
            //check when no internet
            guard let data = data else {
                let internetError = NSError(domain: "Client Error", code: -1, userInfo: [NSLocalizedDescriptionKey : "Timeout occurred during the communication."])
                DispatchQueue.main.async {
                    completion(nil, internetError)
                }
                return
            }
            guard let dataString = String(data: data, encoding: String.Encoding.utf8) else { return }
            guard let decodedDataString = dataString.removingPercentEncoding else { return }
            
            if let data = decodedDataString.data(using: .utf8) {
                
                #if DEBUG
                print("\n✂︎----------------------------------------------------------")
                print("From API URL: \(request.url!)")
                print("Response Data:\n\(String(describing: self.JSONStringify(value: try! self.jsonToDic(data), prettyPrinted: true)))")
                print("✂︎----------------------------------------------------------\n")
                #endif
               
                
            }
            }.resume()
    }
    
    @objc private func onTimeOut() {
        isTimeout = true
    }
    
   
    private func JSONStringify(value: AnyObject, prettyPrinted: Bool = false) -> String {
        let options = prettyPrinted ? JSONSerialization.WritingOptions.prettyPrinted : nil
        if JSONSerialization.isValidJSONObject(value) {
            if let data = try? JSONSerialization.data(withJSONObject: value, options: options!) {
                if let string = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
                    return string as String
                }
            }
        }
        return ""
    }
    
    private func jsonToDic(_ data: Data) throws -> NSDictionary{
        enum JSONError: Error {
            case serializedJSONError // dic -> JSON
            case deserializedJSONError
        }
        
        guard let dic: NSDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary else{
            throw JSONError.deserializedJSONError
        }
        
        return dic
    }
    
    private func dicToJSONString(_ dic: [String:AnyObject]) -> String? {
        guard let data: Data = try? JSONSerialization.data(withJSONObject: dic, options: JSONSerialization.WritingOptions(rawValue: 0)) else{
            return nil
        }
        
        guard let jsonString: String = String(data: data, encoding: String.Encoding.utf8) else{
            return nil
        }
        return jsonString
    }
    
    private func encode(_ string:String) -> String? {
        let allowedCharacterSets = (NSCharacterSet.urlQueryAllowed as NSCharacterSet).mutableCopy() as! NSMutableCharacterSet
        allowedCharacterSets.removeCharacters(in: "!@#$%^&*()-_+=~`:;\"'<,>.?/")
        guard let encodeString = string.addingPercentEncoding(withAllowedCharacters: allowedCharacterSets as CharacterSet) else{
            return nil
        }
        return encodeString
    }
    
}
